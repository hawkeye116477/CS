#!/usr/bin/env python3
# pylint: disable=C0103
import os
import shutil
import xml.etree.ElementTree as ET

pj = os.path.join
pn = os.path.normpath

script_path = os.path.dirname(os.path.realpath(__file__))
main_path = script_path
temp_path = pj(main_path, "src_temp")

if os.path.exists(temp_path):
    shutil.rmtree(temp_path)

os.chdir(main_path)

shutil.copytree(pn("./src"), temp_path)
shutil.copy(pn("license.txt"), temp_path)

os.chdir(temp_path)

unused_locales = []

with open(pj(temp_path, "locales.manifest"), "r", encoding='utf-8') as l_m:
    for line in l_m:
        if "#locale" in line:
            unused_locales.append(line.split()[2])

if unused_locales:
    for ul in unused_locales:
        shutil.rmtree(pn("./locale/" + ul))

# Get extension version from install.rdf
rdfRoot = ET.parse(pn("./install.rdf")).getroot()
emRdf = "http://www.mozilla.org/2004/em-rdf#"
xmlnsRdf =  "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
namespaces = {'xmlns': xmlnsRdf,
              'em': emRdf}
for description in rdfRoot.findall('xmlns:Description', namespaces):
    extVersion = description.get("{"+emRdf+"}version")

# Create xpi
artifacts_path = pj(main_path, "artifacts")
f_name = "CS-"+extVersion
UXP_xpi = pj(artifacts_path, f_name + ".xpi")
if os.path.exists(UXP_xpi):
    print("Removing old xpi file")
    os.remove(UXP_xpi)
if not os.path.exists(artifacts_path):
    os.makedirs(artifacts_path)
print("Creating new xpi file in artifacts directory")
shutil.make_archive(pj(artifacts_path, f_name), 'zip', "./")
os.rename(pj(artifacts_path, f_name + ".zip"), UXP_xpi)

# Cleanup
os.chdir(main_path)
shutil.rmtree(temp_path)

# Update update.xml
print("Updating update.xml")
updateXml = pn("./update/update.xml")
updateXmlTree = ET.parse(updateXml)
updateXmlRoot = updateXmlTree.getroot()
ET.register_namespace('em', emRdf)
ET.register_namespace('RDF', xmlnsRdf)
for description in updateXmlRoot.findall('./xmlns:Description/em:updates/xmlns:Seq/xmlns:li/xmlns:Description', namespaces):
    description.set("{"+emRdf+"}version", extVersion)
    desc = description.find(
        "./em:targetApplication/xmlns:Description", namespaces)
    desc.set("{"+emRdf+"}updateLink",
             "https://github.com/hawkeye116477/CS/releases/download/"+extVersion+"/"+f_name+".xpi")
updateXmlTree.write(updateXml, encoding='UTF-8',
                    xml_declaration=True, method='xml')
