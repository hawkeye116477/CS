[![GitHub Releases](https://img.shields.io/github/downloads/hawkeye116477/CS/latest/total?color=green)](https://github.com/hawkeye116477/CS/releases/latest)
[![Crowdin](https://badges.crowdin.net/classic-sidebar/localized.svg)](https://github.com/hawkeye116477/CS/wiki/Help-with-translation)
[![License](https://img.shields.io/badge/License-GPLv2-blue.svg)](https://raw.githubusercontent.com/hawkeye116477/CS/master/license.txt)
===================================================================
Classic Sidebar (CS) - a sidebar extension for Waterfox Classic and Current
===================================================================

Build the extension
-------------------

To build an installable `.xpi` extension for Waterfox Classic:

1. Clone this repository.
2. Execute `./build.py` at the repository root (Python 3.5+ required).
3. Go to the `artifacts` directory and install the resulting `CS-version.xpi` file with Waterfox Classic.


Links
-----

[Setting up an extension development environment](https://udn.realityripple.com/docs/Archive/Add-ons/Setting_up_extension_development_environment)


Issues
-------

Please use the [issue tracker of GitHub](/issues?state=open) or [Reddit](https://www.reddit.com/r/ClassicSidebar) when reporting bugs or enhancements.


License:
--------
Portions &copy; 2020+ hawkeye116477

Portions &copy; 2017+ FranklinDM

Portions &copy; 2005+ Ingo Wennemaring

> GNU General Public License, Version 2.0
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
